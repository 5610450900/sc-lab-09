package Comparable_and_comparator;


public class Product implements Taxable,Comparable<Product>{
	
	private String name;
	private double price;
	
	public Product(String aName, double aPrice){
		this.name = aName;
		this.price = aPrice;
	}

	public double getPrice() {
		return price;
	}
	public double getTax() {
		// TODO Auto-generated method stub
		double Tax = price*7/100;
		return Tax;
	}
	
	@Override
	public int compareTo(Product other) {
		// TODO Auto-generated method stub
		if (this.price < other.price ) { return -1; }
		if (this.price > other.price ) { return 1;  }
		return 0;
	}
	
	public String getName(){
		return this.name;
	}

}
