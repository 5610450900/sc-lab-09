package Comparable_and_comparator;
import java.util.ArrayList;
import java.util.Collections;


public class testCase {
	public static void main(String[] args){
		System.out.println("---Person---");
		ArrayList<Person> personList = new ArrayList<Person>();
		personList.add(new Person("P1",50000));
		personList.add(new Person("P2",100000));
		personList.add(new Person("P3",75000));
		
		System.out.println("-before sort income in Person");
		for (Person ps : personList) 
			System.out.println(ps.getIncome());

		Collections.sort(personList);
		
		System.out.println("After sort income in Person");
		for (Person ps : personList) 
			System.out.println(ps.getIncome());
		
		ArrayList<Product> productList = new ArrayList<Product>();
		productList.add(new Product("PD1",100));
		productList.add(new Product("PD2",150));
		productList.add(new Product("PD3r",200));
		
		System.out.println("\n---Product---");
		System.out.println("Before sort price in Product");
		for (Product pd : productList) 
			System.out.println(pd.getPrice());

		Collections.sort(productList);
		
		System.out.println("-after sort price in Product");
		for (Product pd : productList) 
			System.out.println(pd.getPrice());
		
		 
		System.out.println("\n---Company---");
		ArrayList<Company> companyList = new ArrayList<Company>();
		companyList.add(new Company("C1 ", 250000, 300000));
		companyList.add(new Company("C2 ", 300000, 550000));
		companyList.add(new Company("C3 ", 200000, 25000));
		
		System.out.println("-Before sort");
		for (Company c : companyList) {
			System.out.println(c);
		}
				
		Collections.sort(companyList, new EarningComparator());
		System.out.println("\n-After sort with Earning");
		for (Company c : companyList) {
			System.out.println(c);
		}
		
		Collections.sort(companyList, new ExpenseComparator());
		System.out.println("\n-After sort with Expense");
		for (Company c : companyList) {
			System.out.println(c);
		}

		Collections.sort(companyList, new ProfitComparator());
		System.out.println("\n-After sort with Profit");
		for (Company c : companyList) {
			System.out.println(c);
		}
		
		System.out.println("\n---Taxable of Person, Product, Company---");
		ArrayList<Taxable> taxList = new ArrayList<Taxable>();
		taxList.add(new Person("P1 ",1000000));
		taxList.add(new Product("PD1 " ,100000));
		taxList.add(new Company("PD2 ",400000,50000));
		
		System.out.println("-Before sort");
		for (Taxable t : taxList) {
			System.out.println("Tax of "+t.getName()+" is "+t.getTax());
		}
		
		Collections.sort(taxList, new TaxComparator());
		System.out.println("\n-After sort with Taxable");
		for (Taxable t : taxList) {
			System.out.println("Tax of "+t.getName()+" is "+t.getTax());
		
		}
	}


}