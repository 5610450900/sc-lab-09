package Comparable_and_comparator;

import java.util.Comparator;


public class ProfitComparator implements Comparator<Company>{
	@Override
	public int compare(Company o1, Company o2) {
		if (o1.getProfit() < o2.getProfit()) return -1;
		if (o1.getProfit() > o2.getProfit()) return 1;
		return 0;
	}

}
