package Comparable_and_comparator;

import java.util.Comparator;

public class EarningComparator implements Comparator<Company>{

	public int compare(Company o1, Company o2) {
		if (o1.getEarning() < o2.getEarning()) return -1;
		if (o1.getEarning() > o2.getEarning()) return 1;
		return 0;
	}


}
