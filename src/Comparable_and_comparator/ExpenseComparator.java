package Comparable_and_comparator;

import java.util.Comparator;


public class ExpenseComparator implements Comparator<Company>{

	public int compare(Company o1, Company o2) {
		if (o1.getExpense() < o2.getExpense()) return -1;
		if (o1.getExpense() > o2.getExpense()) return 1;
		return 0;
	}

}
