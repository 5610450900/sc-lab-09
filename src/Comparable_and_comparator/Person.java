package Comparable_and_comparator;


public class Person implements Taxable,Comparable<Person>{
	private String personName;
	private double incomePerYear;
	
	public Person(String aPersonName,double aIncomePerYear){
		this.personName = aPersonName;
		this.incomePerYear = aIncomePerYear;
	}
	public double getTax() {
		double Tax;
		if(incomePerYear>300000){
			Tax = (300000*5/100)+(incomePerYear - 300000)*10/100;	
		}
		else{
			Tax = incomePerYear*5/100;
		}
		return Tax;
	}
	
	public int compareTo(Person other) {
		if (this.incomePerYear < other.incomePerYear ) { return -1; }
		if (this.incomePerYear > other.incomePerYear ) { return 1;  }
		return 0;
	}
	
	public double getIncome(){
		return this.incomePerYear;
		
	}
	
	public String getName(){
		return this.personName;
	}

}
