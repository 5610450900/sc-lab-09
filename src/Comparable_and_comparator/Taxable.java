package Comparable_and_comparator;


public interface Taxable {
	
	double getTax();
	String getName();

}