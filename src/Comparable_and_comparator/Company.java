package Comparable_and_comparator;


public class Company implements Taxable{
	
	private String name;
	private double earning;
	private double expense;
	private double profit;
	
	public Company(String aName, double anEarning, double anExpense){
		this.name = aName;
		this.earning = anEarning;
		this.expense = anExpense;
		if(this.earning > expense){
			this.profit = earning-expense;
		}
		else{
			this.profit = 0;
		}
	}
	public double getTax() {
		// TODO Auto-generated method stub
		double Tax;
		if(earning>expense){
			Tax = (earning - expense)*30/100;
		}
		else{
			Tax = 0;
		}
		return Tax;
	}

	public String getName() {
		return name;
	}

	public double getEarning() {
		return earning;
	}

	public double getExpense() {
		return expense;
	}
	
	public double getProfit() {
		return profit;
	}
	public String toString(){
		return "Company[name="+this.name+", earning="+this.earning+", expense="+this.expense+", profit="+this.profit+"]";
		
	}
}