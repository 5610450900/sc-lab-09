package Tree_traversal;

import java.util.ArrayList;


public class TraversalTest {

	public static void main(String args[]) {
		
		Node n1 = new Node("C",null,null);
		Node n2 = new Node("E",null,null);
		Node n3 = new Node("H",null,null);
		Node n4 = new Node("A",null,null);
		Node n5 = new Node("D",n1,n2);
		Node n6 = new Node("I",n3,null);
		Node n7 = new Node("B",n4,n5);
		Node n8 = new Node("G",null,n6);
		Node n9 = new Node("F",n7,n8);
		
		ReportConsole console = new ReportConsole();
		Traversal post = new PostOrderTraversal();
		Traversal in = new InOrderTraversal();
		Traversal pre = new PreOrderTraversal();
		ArrayList<String> lst1 = console.display(n9, post);
		ArrayList<String> lst2 = console.display(n9, in);
		ArrayList<String> lst3 = console.display(n9, pre);
		
		System.out.print("Traverse with PostOrder : ");
		for (int i = 0;i < lst1.size();i++) {
			System.out.print(lst1.get(i)+" ");
		}
		System.out.println();
		System.out.println();
		System.out.print("Traverse with InOrder : ");
		for (int i = 0;i < lst2.size();i++) {
			System.out.print(lst2.get(i)+" ");
		}
		System.out.println();
		System.out.println();
		System.out.print("Traverse with PreOrder : ");

		for (int i = 0;i < lst1.size();i++) {
			System.out.print(lst1.get(i)+" ");
		}
	}

}
