package Tree_traversal;

import java.util.ArrayList;
import java.util.Stack;

public class PostOrderTraversal implements Traversal {
	
	public ArrayList<String> traverse(Node root) {
		ArrayList<String> postlist = new ArrayList<String>();
		
		if(root == null) {
			return postlist;	
		}
		Stack<Node> stack = new Stack<Node>();
		stack.push(root);
		
		Node prev = null;
		while(!stack.empty()) {
			Node curr = stack.peek();
			
			if(prev == null || prev.getLeftNode() == curr || prev.getRightNode() == curr) {
				if(curr.getLeftNode() != null){
                    stack.push(curr.getLeftNode());
                }else if(curr.getRightNode() != null){
                    stack.push(curr.getRightNode());
                }else{
                    stack.pop();
                    postlist.add(curr.getNode());
                }
			}
			else if(curr.getLeftNode() == prev){
                if(curr.getRightNode() != null){
                    stack.push(curr.getRightNode());
                }else{
                    stack.pop();
                    postlist.add(curr.getNode());
                }
			}
			else if(curr.getRightNode() == prev){
                stack.pop();
                postlist.add(curr.getNode());
            }
 
            prev = curr;
		}
		
		return postlist;
		
		
		
	}

}
