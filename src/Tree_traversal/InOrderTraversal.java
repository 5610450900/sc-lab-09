package Tree_traversal;

import java.util.ArrayList;
import java.util.Stack;


public class InOrderTraversal implements Traversal {

	public ArrayList<String> traverse(Node root) {
		ArrayList<String> inlist = new ArrayList<String>();
		
		if(root == null) {
			return inlist;
		}
		
		Stack<Node> stack = new Stack<Node>();
		Node prev = root;
		while(!stack.isEmpty() || prev != null) {
			if(prev != null) {
				stack.push(prev);
				prev = prev.getLeftNode();
			}
			
			else {
				Node top = stack.pop();
				inlist.add(top.getNode());
				prev = top.getRightNode();
			}
		}
		return inlist;
	}

}
