package Tree_traversal;

import java.util.ArrayList;

public interface Traversal {
	
	ArrayList<String> traverse(Node node);

}
