package Tree_traversal;

public class Node {
	private String node;
	private Node nodeleft;
	private Node noderight;
	
	public Node(String node,Node nodeleft,Node noderight) {
		this.node = node;
		this.nodeleft = nodeleft;
		this.noderight = noderight;
	}
	public String getNode() {
		return node;
	}
	public Node getLeftNode() {
		return nodeleft;
	}
	public Node getRightNode() {
		return noderight;
	}

}
